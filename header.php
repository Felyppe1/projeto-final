<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <title>Document</title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header class='header'>
        <?php the_custom_logo(); ?>
        <form class='lp_search_div' action="<?php bloginfo('url'); ?>/">
            <button type="submit"><img src="<?php echo IMAGES_DIR . '/icone-lupa.png' ?>" alt="Ícone da lupa"></button>
            <input type="text" name="s" id="s" autocomplete='off'>
            <input type="text" name='post_type' id='post_type' value='product' class='hidden'>
        </form>
        <!-- <div class='lp_search_div'>
            <label for="lp_search_input"><img src="http://comesbebes.local/wp-content/uploads/2023/02/icone-lupa.png" alt="Ícone da lupa"></label>
            <input type="text" name="lp_search_input" id="lp_search_input">
        </div> -->
        <div>
            <button class='make_order_button'><a href="<?php echo esc_url( get_permalink( wc_get_page_id( 'shop' ))); ?>">Faça um pedido</a></button>
            <div><img class='header_cart' onclick='abrirCarrinho()' src="<?php echo IMAGES_DIR . '/icone-carrinho.png' ?>" alt="Ícone do carrinho"></div>
            <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')) ?>"><img src="<?php echo IMAGES_DIR . '/icone-usuario.png' ?>" alt="Ícone do usuário"></a>
        </div>
        
        
    </header>
    <!-- MINI CARRINHO (css dele tá na header.css)-->
    <div class='comesbebes_mini_cart'>
        <img class='close_mini_cart' onclick='fecharCarrinho()' src="<?php echo IMAGES_DIR . '/x.png' ?>" alt="Ícone de fechar">
        <h1>CARRINHO</h1>
        <div class='mini_cart_line'></div>  <!-- a outra tá no mini-cart.php -->
        <?php
        woocommerce_mini_cart();
        ?>
    </div>
