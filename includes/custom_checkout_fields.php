<?php
function custom_override_checkout_fields($fields) {
    $fields['billing']['billing_email'] = [
        'label' => 'Email para contato',
        'required' => true,
        'priority' => 5,
        'class' => array( 'ch_email_field' ),
        'placeholder' => 'Digite seu email'
    ];
    $fields['billing']['billing_first_name'] = [
        'label' => 'Nome',
        'required' => true,
        'priority' => 10,
        'class' => array( 'ch_first_name_field', 'half_field' ),
        'placeholder' => 'Digite seu nome'
    ];
    $fields['billing']['billing_last_name'] = [
        'label' => 'Sobrenome',
        'required' => true,
        'priority' => 20,
        'class' => 'ch_last_name_field',
        'placeholder' => 'Digite seu sobrenome'
    ];
    $fields['billing']['billing_phone'] = [
        'label' => 'Telefone fixo',
        'required' => false,
        'priority' => 25,
        'class' => array( 'half_field'),
        'placeholder' => '(21) XXXX-XXXX'
    ];
    $fields['billing']['billing_cellphone'] = [
        'label' => 'Celular',
        'required' => true,
        'priority' => 28,
        'class' => array( 'half_field'),
        'placeholder' => '(21) XXXXX-XXXX'
    ];
    $fields['billing']['billing_postcode_1'] = [
        'label' => 'CEP',
        'required' => true,
        'priority' => 29,
        'placeholder' => 'XXXXX-XXX'
    ];
    $fields['billing']['billing_address'] = [
        'label' => 'Logradouro',
        'required' => true,
        'priority' => 35,
        'placeholder' => 'Rua Lorem Ipsum, 150, pro'
    ];
    $fields['billing']['billing_complement'] = [
        'label' => 'Complemento',
        'required' => false,
        'priority' => 40,
        'placeholder' => 'BI 2, Apto 905'
    ];
    $fields['billing']['billing_neighborhood'] = [
        'label' => 'Bairro',
        'required' => true,
        'priority' => 45,
        'class' => array( 'half_field'),
        'placeholder' => 'Lorem Ipsum'
    ];
    $fields['billing']['billing_city'] = [
        'label' => 'Cidade',
        'required' => true,
        'priority' => 70,
        'class' => array( 'half_field'),
        'placeholder' => 'Lorem Ipsum'
    ];
    /* $fields['billing']['billing_state'] = [
        'type' => 'state',
        'label' => 'Estado',
        'validate' => array('state'),
        'priority' => 1,
        'class' => array( 'form-row-wide', 'address-field', 'hidden'),
        'country_fields' => 'billing_country',
        'country' => 'BR',
    ]; */
    $fields['billing']['billing_country'] = [
        'type' => 'country',
        'label' => 'País',
        'class' => array( 'form-row-wide', 'address-field', 'update_totals_on_change', 'hidden')
    ];
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_2']);
    /* echo '<pre>';
    print_r($fields);
    echo '</pre>'; */
    return $fields;
}

function add_div_after_form() {
    ?>
    <div class='payment_div'>
        <h2>Informações de pagamento</h2>
        <div>
            <div class='payment_options_div'>
                <p>Forma de pagamento</p>
                <div>
                    <div class='money_div'>
                        <img src="<?php echo IMAGES_DIR . '/icone-caminhao.png' ?>" alt="">
                        <div>
                            <p>Dinheiro</p>
                            <p>Na entrega</p>
                        </div>
                    </div>
                    
                    <div class='card_div'>
                        <img src="<?php echo IMAGES_DIR . '/icone-cartao.png' ?>" alt="">
                        <p>Cartão</p>
                    </div>
                </div>
            </div>
            <div class='chose_card'>
                <div class='card_number_div'>
                    <p>Número do cartão</p>
                    <input type="text" placeholder='1111.1111.1111.1111'>
                </div>
                <div class='validity_cvv_div'>
                    <div class='validity_div'>
                        <p>Validade do cartão</p>
                        <input type="text" placeholder='12/12/2000'>
                    </div>
                    <div class='cvv_div'>
                        <p>CVV</p>
                        <input type="text" placeholder='123'>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <script>
        function mostrarDiv() {
            let chose_card = document.querySelector('.chose_card')
            chose_card.classList.remove('hidden')
            console.log('cliquei no card')
        }

        function excluirDiv() {
            let chose_card = document.querySelector('.chose_card')
            chose_card.classList.add('hidden')
            console.log('cliquei no money')
        }

        let card_div = document.querySelector('.card_div')
        card_div.addEventListener('click', ()=>mostrarDiv())

        let money_div = document.querySelector('.money_div')
        money_div.addEventListener('click', ()=>excluirDiv())
    </script>
    <?php
}