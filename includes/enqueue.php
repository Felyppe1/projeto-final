<?php

function enqueue_comesebebes_styles() {
    wp_register_style('style', get_stylesheet_uri(), [], '1.0.0', false);
    wp_register_style('comesebebes-roboto-font', "https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap");
    wp_register_style('comesebebes-bellota-font', "https://fonts.googleapis.com/css2?family=Bellota+Text:wght@300;400;700&display=swap");
    wp_register_style('comesebebes-header', STYLES_DIR . '/header.css', [], '1.0.0', false);
    wp_register_style('comesebebes-footer', STYLES_DIR . '/footer.css', [], '1.0.0', false);
    wp_register_style('category-div-style', STYLES_DIR . '/category.css', [], '1.0.0', false);
    wp_register_style('product-div-style', STYLES_DIR . '/product.css', [], '1.0.0', false);
    wp_register_style('page-endereco-style', STYLES_DIR . '/styleEndereco.css', [], '1.0.0', false);

    wp_register_style('archive-product-style', STYLES_DIR . '/archive-product.css', [], '1.0.0', false);
    wp_register_script('cart-script', SCRIPTS_DIR . '/cart-event.js', [], '1.0.0', true);

    wp_register_style('formatar-image', STYLES_DIR . '/format-image.css', [], '1.0.0', false);

    wp_enqueue_style('style');
    wp_enqueue_style('comesebebes-roboto-font');
    wp_enqueue_style('comesebebes-bellota-font');
    wp_enqueue_style('comesebebes-header');
    wp_enqueue_style('comesebebes-footer');
    wp_enqueue_style('category-div-style');
    wp_enqueue_style('product-div-style');
    wp_enqueue_style('page-endereco-style');
    wp_enqueue_style('formatar-image');


    wp_enqueue_script('cart-script');

    if (is_page('home')) {
        wp_register_style('page-home-style', STYLES_DIR . '/page-home.css', [], '1.0.0', false);

        wp_enqueue_style('page-home-style');
    }

    if (is_post_type_archive('product')) {
        wp_enqueue_style('archive-product-style');
    }

    if (is_tax('product_cat') || is_page('precos')) {
        wp_enqueue_style('archive-product-style');
    }

    if(is_product()){
        wp_register_style('single-product-style', STYLES_DIR . '/single-product.css', [], '1.0.0', false);

        wp_enqueue_style('single-product-style');
    }
    
    if(is_page(get_option('woocommerce_myaccount_page_id') )){
        wp_register_style('my-account-style', STYLES_DIR . '/my-account.css', [], '1.0.0', false);
        wp_register_style('page-myaccount-style', STYLES_DIR . '/page-my-account.css', [], '1.0.0', false);
        wp_register_style('orders-style',  STYLES_DIR . '/orders.css', [], '1.0.0', false);

        wp_enqueue_style('orders-style');
        wp_enqueue_style('my-account-style');
        wp_enqueue_style('page-myaccount-style');
    }
    
    if(is_page('my-account-my-address')){
        wp_enqueue_style('page-endereco-style');
    }
    if (is_checkout()) {
        wp_register_style('page-checkout-style', STYLES_DIR . '/checkout.css', [], '1.0.0', false);

        wp_enqueue_style('page-checkout-style');
    }

    $variavel_teste = 'console.log("TESTE")';

    wp_add_inline_script(
        'cart-script',
        $variavel_teste,
        'before'
    );

    /* $caraca = 'teste';

    $comidas_carrinho = WC()->cart->get_cart();
    foreach ($comidas_carrinho as $item) {
        // access the values of the array like this:
        $key = $item['key'];
        $product_id = $item['product_id'];
        $variation_id = $item['variation_id'];
    
        // do something with the values
        wp_localize_script( 'cart-script', 'comidas', $comidas_carrinho );
    } */
    

    
}

?>