<?php

function setup_restaurant_theme() {
    add_theme_support('custom-logo');
    add_theme_support('menus');
    add_theme_support('woocommerce');
    register_nav_menus([
        'painel' => 'menu do painel de usuario'
    ]);
}

?>