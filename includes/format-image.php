<?php

function format_image_product($product){
    ?>
    <div class="content-comida">
        <div class="image"><?php echo $product->get_image();?></div>
            <div class="sobre-produto">
                <h1 class="title"><?php echo $product->get_name(); ?></h1>
                <h4 class="description"><?php echo $product->get_description(); ?></h4>
        
        <div class="botao_opcao">
            <div class="quantity">
           
                <button type="button" class="menos" onclick="deleta">-</button>
                
                <input type="text" value="0" readonly="" name="quantidade_produto" class="quantidade">
                
                <button type="button" class="mais" onclick="add">+</button>
            </div>

                <form class="porcao" action="post">
                    <select name="porcao" id="porcao-select">
                        <option value="">Selecione a porção</option>
                        <option value="pqno">Pequeno</option>
                        <option value="medio">Medio</option>
                        <option value="grande">Grande</option>
                    </select>
                </form>
        </div>
        <div class="preco_botao">
            <h4 class="preco"><?php echo $product->get_price_html(); ?></h4>

            <!-- <a href="<?php echo $product->get_permalink(); ?>" class="botao-adicionar-single-product">ADICIONAR</a> -->
            <form class='cart' method='post' enctype='multipart/form-data'>
                <button type="submit" name='add-to-cart'
                value='<?php echo $product->get_id(); ?>' class=' botao-adicionar-single-product'>ADICIONAR</button>
            </form>
        </div>       
    </div>
</div>
<?php
}

?>
