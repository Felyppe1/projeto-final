<?php
    function remove_shipping_fields($address_shipping_fields){
        unset($address_shipping_fields['company']);
        unset($address_shipping_fields['state']);
        unset($address_shipping_fields['address_2']);
        unset($address_shipping_fields['country']);
        $address_shipping_fields['address_1']['label'] = 'Logradouro';
        $address_shipping_fields['address_1']['placeholder'] = 'Rua e número';
        $address_shipping_fields['city']['priority'] = 80;
        $address_shipping_fields['city']['class'] = ['form-row-last'];
        $address_shipping_fields['city']['placeholder'] = 'Sua cidade';
        $address_shipping_fields['postcode']['priority'] = 30;
        $address_shipping_fields['postcode']['placeholder'] = 'Digite seu CEP';
        $address_shipping_fields['first_name']['placeholder'] = 'Digite seu nome';
        $address_shipping_fields['last_name']['placeholder'] = 'Digite seu sobrenome';
    
    
       
        return $address_shipping_fields;
    
    }
    
    function add_shipping_fields($address_shipping_fields){
        $address_shipping_fields['bairro'] = array(
            'label' => 'Bairro',
            'required' => true,
            'class' => array('form-row-first'),
            'priority' => 70,
            'placeholder' => 'Seu bairro',
        );
        $address_shipping_fields['complemento'] = array(
            'label' => 'Complemento',
            'required' => true,
            'class' => array('form-row-wide'),
            'priority' => 50,
            'placeholder' => 'Complemento do seu endereço',
        );
        return $address_shipping_fields;
    }
    

    
    function change_shipping_title($title){
        return 'Novo endereço';
    }
    

    
    function change_address_description($description){
        return 'Os endereços a seguir serão usados na página de finalizar pedido como endereços padrões, mas é possível modificá-los durante a finalização do pedido.';
    }
    
  
    
    
    function custom_address_formats( $formats ) {
        $formats[ 'default' ]  = "
        <div class='all-address-container-enderecos'>
            <div class='all-address-name'> Fulano der tal </div>
            <div class='all-address-cep'>2222-222222 </div>
            <div class='all-address-endereco'>Rua Lorem Ipsum, 132, Bl2, Apto 1003
            Estado Lorem Ipsum \n Cidade de Lorem Ipsum \n Estado Lorem Ipsum </div>   
        </div>"
        ;
        return $formats;
    }
    


  
    function woocommerce_single_variation() {
        echo '<h2 class="single-product-price woocommerce-variation single_variation"></h2>';
    }



?>