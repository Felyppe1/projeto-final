<?php
function new_orders_columns( $columns = array() ) {

    if( isset($columns['order-total']) ) {
        unset( $columns['order-number'] );
        unset( $columns['order-date'] );
        unset( $columns['order-actions'] );
        unset($columns['order-status']);
        unset($columns['order-total']);
    }

    $columns['order-number'] = 'Pedido';
    $columns['order-date'] = 'Data';
    $columns['order-status'] = 'Status';
    $columns['order-total'] = 'Total';
    $columns['order-actions'] = '';
    return $columns;
}

function new_status_to_list( $order_statuses ) {

    $order_statuses['wc-pending'] = 'Pendente';
    $order_statuses['wc-processing'] = 'Processando';
    $order_statuses['wc-on-hold'] = 'Em espera';
    $order_statuses['wc-completed'] = 'Completado';
    $order_statuses['wc-cancelled'] = 'Cancelado';
    $order_statuses['wc-refunded'] = 'Refundado';
    $order_statuses['wc-failed'] = 'Falhou';
	return $order_statuses;
}

function new_order_actions($actions, $order){
    if (! $order->needs_payment() ) {
		$actions['view']['name'] = 'Visualizar';
    } else{
        $actions['pay']['name'] = 'Pagar';
    }
    
    if(in_array( $order->get_status(), apply_filters( 'woocommerce_valid_order_statuses_for_cancel', array( 'pending', 'failed' ), $order ), true )){
        $actions['cancel']['name'] = 'Cancelar';
    }

    return $actions;
}

function new_date_format(){
    $new_date_format = 'd/m/y';
    return $new_date_format;
}
?>