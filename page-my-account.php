<?php
get_header();
?>
<?php wp_nav_menu(['menus' => 'painel']);?>

<?php
if (have_posts()) { 
    while(have_posts()) {
        the_post();
        ?> 
        <main>
            <?php the_content(); ?>
        </main>
        <?php
    }
}

get_footer();
?>