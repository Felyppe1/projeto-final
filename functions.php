<?php

//Variáveis
define('ROOT_DIR', get_theme_file_path());
define('STYLES_DIR', get_template_directory_uri() . '/assets/css');
define('IMAGES_DIR', get_template_directory_uri() . '/assets/images');
define('SCRIPTS_DIR', get_template_directory_uri() . '/assets/js');
define('INCLUDES_DIR', ROOT_DIR . '/includes');

//Includes
include_once(INCLUDES_DIR . '/enqueue.php');
include_once(INCLUDES_DIR . '/theme-setup.php');
include_once(INCLUDES_DIR . '/remove-account-display.php');
include_once(INCLUDES_DIR . '/format-product.php');
include_once(INCLUDES_DIR . '/custom-address.php');
include_once(INCLUDES_DIR . '/format-image.php');

include_once(INCLUDES_DIR . '/custom_order_table.php');
include_once(INCLUDES_DIR . '/custom_checkout_fields.php');

//Ganchos
add_action('wp_enqueue_scripts', 'enqueue_comesebebes_styles');
add_action('after_setup_theme', 'setup_restaurant_theme');

//Modificando as opções do filtro
add_filter('woocommerce_save_account_details_required_fields', 'remove_required_display');
add_filter( 'woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby' );  
function custom_woocommerce_catalog_orderby( $sortby ) {
    $sortby['price'] = __( 'Preço (crescente)', 'woocommerce' );
    $sortby['rating'] = __( 'Classificação', 'woocommerce' );
    $sortby['price-desc'] = __( 'Preço (descrescente)', 'woocommerce');
    unset( $sortby['menu_order'], $sortby['popularity'], $sortby['date'] );
    return $sortby;
}
add_filter( 'woocommerce_account_orders_columns', 'new_orders_columns' );
add_filter( 'wc_order_statuses', 'new_status_to_list' );
add_filter('woocommerce_my_account_my_orders_actions', 'new_order_actions', 11, 2);
add_filter('woocommerce_date_format', 'new_date_format');
// Remoção de ganchos
remove_action( 'woocommerce_account_navigation', 'woocommerce_account_navigation' );

add_action('loop_shop_per_page', 'comidas_loop_shop_per_page');
function comidas_loop_shop_per_page() {
    return 8;
}

//Editar a string do valor total do mini carrinho
add_action( 'woocommerce_widget_shopping_cart_total', 'woocommerce_widget_shopping_cart_subtotal');
function woocommerce_widget_shopping_cart_subtotal( $subtotal ) {
    echo esc_html__( 'Total do Carrinho: ', 'woocommerce' ) . WC()->cart->get_cart_subtotal();
}

//Remover a opção de ver carrinho do mini carrinho
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10 );

//Editar a string do botão de finalizar compra
add_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20);
function woocommerce_widget_shopping_cart_proceed_to_checkout() {
    $wp_button_class = wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '';
    echo '<a href="' . esc_url( wc_get_checkout_url() ) . '" class="button checkout wc-forward' . esc_attr( $wp_button_class ) . '">' . esc_html__( 'COMPRAR', 'woocommerce' ) . '</a>';
}


add_filter( 'woocommerce_checkout_fields', 'custom_override_checkout_fields');  //Customizar o formulário na /includes/custom_checkout_fields.php

add_action( 'woocommerce_after_checkout_billing_form', 'add_div_after_form' );  //Adiciona div do Informações do Pagamento

add_filter('woocommerce_default_address_fields','remove_shipping_fields');


add_filter('woocommerce_default_address_fields','add_shipping_fields');



add_filter( 'woocommerce_my_account_edit_address_title','change_shipping_title');



add_filter('woocommerce_my_account_my_address_description','change_address_description');



add_filter('woocommerce_localisation_address_formats', 'custom_address_formats');



//teste


//add_filter( 'filter_dishes_price')

?>