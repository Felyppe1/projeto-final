<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$allowed_html = array(
	'a' => array(
		'href' => array(),
	),
);
?>
<div class="welcome">
	<p>
		<?php
		printf(
			/* translators: 1 = nome do usuario 2 = log out */
			wp_kses( 'Olá %1$s (Não é %1$s? <a href="%2$s">Sair</a>)', $allowed_html ),
			'<strong>' . esc_html( $current_user->display_name ) . '</strong>',
			esc_url(wp_logout_url())
		);
		?>
	</p>
	<br>
	<p>
		<?php
		/* translators: 1: Orders URL 2: Address URL 3: Account URL. */
		$dashboard_desc ='A partir do painel de controle de sua conta, você pode ver suas <a href="%1$s">compras recentes</a>, gerenciar seus <a href="%2$s">endereços de entrega e faturamento</a>, e editar sua senha e detalhes da conta.';
		if ( wc_shipping_enabled() ) {
			/* translators: 1: Orders URL 2: Addresses URL 3: Account URL. */
			$dashboard_desc ='</br>' . 'A partir do painel de controle de sua conta, você pode ver suas <a href="%1$s">compras recentes</a>, gerenciar seus <a href="%2$s">endereços de entrega e faturamento</a>, e editar sua senha e detalhes da conta.';
		}
		printf(
			wp_kses( $dashboard_desc, $allowed_html ),
			esc_url( wc_get_endpoint_url( 'orders' ) ),
			esc_url( wc_get_endpoint_url( 'edit-address' ) )
		);
		?>
	</p>
</div>

<form class="formulario" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >

	<div class="display-name">
		<p class="nome">
			<label for="account_first_name">Nome<span class="required"></span></label>
			<br>
			<input class='info' type="text" class="" name="account_first_name" id="account_first_name" autocomplete="given-name" placeholder = 'Digite seu nome'value="<?php echo $current_user->first_name; ?>" />
		</p>
		<p class="nome">
			<label for="account_last_name">Sobrenome<span class="required"></span></label>
			<br>
			<input class='info' type="text"  name="account_last_name" id="account_last_name" autocomplete="family-name" placeholder='Digite seu sobrenome' value="<?php echo  $current_user->last_name; ?>" />
		</p>
	</div>
	<div class="clear"></div>

	<p class="dados">
		<label for="account_email">Email<span class="required"></span></label>
		<br>
		<input class='info' type="email"  name="account_email" id="account_email" autocomplete="email" placeholder='Digite seu email' value="<?php echo $current_user->user_email; ?>" />
	</p>

	<p class="dados">
		<label for="password_current">Senha atual (deixe em branco para não alterar):</label>
		<br>
		<input class='info' type="password" placeholder='Digite sua senha atual' name="password_current" id="password_current" autocomplete="off" />
	</p>
	<p class="dados">
		<label for="password_1">Nova senha (deixe em branco para não alterar)</label>
		<br>
		<input class='info' type="password" placeholder='Digite sua nova senha' name="password_1" id="" autocomplete="off" />
	</p>
	<p class="dados">
		<label for="password_2">Confirmar nova senha</label>
		<br>
		<input class='info' type="password" placeholder='Confirme sua nova senha' name="password_2" id="password_2" autocomplete="off" />
	</p>

	<div class="clear"></div>

		<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
		<button type="submit" class="registro" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>">SALVAR ALTERAÇÕES</button>
		<input type="hidden" name="action" value="save_account_details" />

</form>
