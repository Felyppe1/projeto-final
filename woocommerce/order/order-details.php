<?php

defined( 'ABSPATH' ) || exit;

$order = wc_get_order( $order_id );

if ( ! $order ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
	wc_get_template(
		'order/order-downloads.php',
		array(
			'downloads'  => $downloads,
			'show_title' => true,
		)
	);
}
?>
<section class="pagina-detalhes">

	<h1 class="pedido-titulo"><?php echo 'DETALHES DO PEDIDO'; ?></h1>

	<div class='detalhes'>
		
		<div class='produto'>
			<h2 class='pedido-subtitulo'><?php echo 'Produto'; ?></h2>
			<div class="itens">
				<?php
				foreach ( $order_items as $item_id => $item ) {
					$product = $item->get_product_id();
					$qty = $item->get_quantity();
					$name = $item->get_name();

					?>
					
					<a href='<?php echo get_permalink($product) ?>'>
					<?php
						echo($name . ' x ' . $qty);
					?>
					</a>
					<?php
				}
				?>
			</div>
		</div>
		<div class='pagamento'>
			<h2 class='pedido-subtitulo'><?php echo 'Metodo de pagamento'; ?></h2>
			<p class='metodo'><?php $order->get_payment_method();?></p>
		</div>
	</div>

			<div class='valores'>
			<?php
			foreach ( $order->get_order_item_totals() as $key => $total ) {
				?>
					<div class='valor'>
						<h2><?php echo $total['label']; ?></h2>
						<p><?php echo ( 'payment_method' === $key ) ? $total['value'] : wp_kses_post( $total['value'] );?></p>
					</div>
					<?php
			}
			?>
			<?php if ( $order->get_customer_note() ) : ?>
				<div>
					<h2 class='pedido-subtitulo'><?php echo 'Anotação:'; ?></h2>
					<p><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></p>
				</div>
			<?php endif; ?>
		</div>

</section>

<?php
if ( $show_customer_details ) {
	wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
}
