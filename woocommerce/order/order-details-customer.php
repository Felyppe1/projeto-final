<?php
defined( 'ABSPATH' ) || exit;
?>
<section class="dados-entrega">

	<h2 class="pedido-subtitulo"><?php echo 'Endereço de entrega'; ?></h2>

	<address class='entrega-endereco'>
		<?php echo ($order->get_formatted_billing_address())?>
	</address>
	<div class='contatos'>
		<p><?php echo esc_html( $order->get_billing_phone() ); ?></p>
		<p><?php echo esc_html( $order->get_billing_email() ); ?></p>
	</div>
</section>


	
