<?php 

get_header();
?>
<main class="sp">
    <?php
        if(have_posts()){
            while(have_posts()){
                the_post();
                $id = get_the_ID();
                $sp_single_products = wc_get_product($id);
                
                format_image_product(wc_get_product($id));
            }
        }
    ?>

<?php

    $related_products = wc_get_products([
        'limit' => 16
    ]);

?>
    <section class="related products">

        <?php
            $heading = apply_filters( 'woocommerce_product_related_products_heading', __( 'MAIS COMIDAS RELACIONADAS', 'woocommerce' ) );

            if ( $heading ) :
        ?>
            <h2><?php echo esc_html( $heading ); ?></h2>
        <?php endif; ?>

            <?php woocommerce_product_loop_start(); ?>
                <div class='sp_whole_category_div'>
                    <?php foreach ( $related_products as $related_product ) : ?>
                        <?php
                            //ALTEREI AQUI DENTRO
                            if ($related_product->get_category_ids()[0] ==              $sp_single_products->get_category_ids()[0]) {
                        ?>
                                <div class='wc_dish'>
                                    <?php echo $related_product->get_image(); ?>
                                    <!-- echo wp_get_attachment_image_src($prato->image_id)[0]; -->
                                        <div class='wc_dish_infos'>
                                            <p> <?php echo $related_product->get_name(); ?></p>
                                            <div>
                                                <p><?php echo $related_product->get_price_html(); ?></p>
                                                <a href="<?php echo $related_product->get_permalink(); ?>">
                                                <img src="<?php echo IMAGES_DIR . '/icone-add-carrinho.png' ?>" alt="">
                                                </a>
                                            </div>
                                        </div>
                                </div>
                            <?php
                            }
                        ?>
                    <?php endforeach; ?>
                </div>         
                <?php woocommerce_product_loop_end(); ?>
    </section>
</main>
<?php
get_footer();

?>