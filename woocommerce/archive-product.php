<?php
get_header();
?>
<main>
    <section class='listap_section1'>
        <div class='listap_whole_category_div'>
            <h2>SELECIONE UMA CATEGORIA</h2>
            <div>
                <div class='category northern'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Nordestina.png" alt="">
                    <p>NORDESTINA</p>
                </div>
                <div class='category vegan'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Vegana.png" alt="">
                    <p>VEGANA</p>
                </div>
                <div class='category pastas'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Massas.png" alt="">
                    <p>MASSAS</p>
                </div>
                <div class='category japenese'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Japonesa.png" alt="">
                    <p>JAPONESA</p>
                </div>
            </div>
        </div>
        
    </section>

    <section class='listap_section2'>
        <div class='filters_div'>
            <h2>PRATOS</h2>
            <h3>
                <?php
                if (is_product_category()) {
                    $category = get_queried_object();
                    $category2 = $category->name;
                    echo 'COMIDA ' . strtoupper($category2);
                }       
                ?>
            </h3>
            <div class='searchfor_div'>
                <p>Buscar por nome:</p>
                <form class='listap_search_div' action="<?php bloginfo('url'); ?>/loja/">  <!-- Tive que botar o /loja/ para funcionar -->
                    <input type="text" name="s" id="s" autocomplete='off'>
                    <!-- <input type="text" name='post_type' id='post_type' value='product' class='hidden'> -->
                </form>
            </div>
            <div class='whole_filters_div'>
                <div class='orderby_div'>
                    <p>Ordenar por:</p>
                    <?php woocommerce_catalog_ordering(); ?>
                </div>
                <div class='filter_price_div'>
                    <p>Filtro de preço:</p>
                    <form class='filter_price_form' action="<?php bloginfo('url') ?>/precos" method='GET'>
                        <div>
                            <label for="from">De:</label>
                            <input type="number" name="from" id="from">
                        </div>
                        <div>
                            <label for="to">Até:</label>
                            <input type="number" name="to" id="to">
                        </div>
                        <button class='hidden' type="submit"></button>
                    </form>
                </div>
            </div>
        </div>  
    </section>

    <section class='listap_section3'>
        <div class='listap_dishes'>
            <?php
            if (!is_page('precos')) {
                if (have_posts()) { 
                    while(have_posts()) {
                        the_post();
                        $id = get_the_ID();
                        $dishes = wc_get_product($id);
                        formatar_produto(wc_get_product($id));
                    }
                }
            }
            else {
                if (have_posts()) {
                    while(have_posts()) {
                        the_post();
                        $from = $_GET['from'];
                        $to = $_GET['to'];
                        $all_dishes = wc_get_products([
                            'limit' => 50
                        ]);
                        foreach ($all_dishes as $dish) {
                            if (($dish->get_price() >= $from) && ($dish->get_price() <= $to)) {
                                formatar_produto($dish);
                            }
                        }     
                    }
                }
            }
            
            ?>
            </div>
    </section>
    <?php 
    echo get_the_posts_pagination( array(
    'prev_text' => '<',
    'next_text' => '>'
    ));
    ?>
</main>
<?php



?>
<?php
get_footer();
?>