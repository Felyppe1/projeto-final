<?php
get_header();
?>

<main>
    <section class='lp_section1'>
        <h1><?php bloginfo('name'); ?></h1>
        <p>O restaurante para todas as fomes</p>
    </section> 
    
    <section class='lp_section2'>
        <h2>CONHEÇA NOSSA LOJA</h2>
        <div class='lp_whole_category_div'>
            <h3>Tipos de pratos principais</h3>
            <div class='category_div'>
                <div class='category northern'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Nordestina.png" alt="">
                    <p>NORDESTINA</p>
                </div>
                <div class='category vegan'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Vegana.png" alt="">
                    <p>VEGANA</p>
                </div>
                <div class='category pastas'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Massas.png" alt="">
                    <p>MASSAS</p>
                </div>
                <div class='category japenese'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Japonesa.png" alt="">
                    <p>JAPONESA</p>
                </div>

                <!-- <div class='category northern'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Nordestina.png" alt="">
                    <p>NORDESTINA</p>
                </div>
                <div class='category vegan'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Vegana.png" alt="">
                    <p>VEGANA</p>
                </div>
                <div class='category pastas'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Massas.png" alt="">
                    <p>MASSAS</p>
                </div>
                <div class='category japenese'>
                    <img src="http://comesbebes.local/wp-content/uploads/2023/02/Japonesa.png" alt="">
                    <p>JAPONESA</p>
                </div> -->
            </div>
        </div>
        <div class='whole_dishes_of_the_day'>
            <h3>Pratos do dia de hoje:</h3>
            <?php $week_day = date_i18n( 'l' ); ?>
            <p><?php echo $week_day;?></p>
            <div class='dishes_of_the_day'>
                <?php 
                $pratos = wc_get_products([
                    'limit' => 4,
                    'orderby' => 'rand',
                    'tag' => 'do dia'
                ]);
                foreach ($pratos as $prato) {
                    /* echo '<pre>';
                    print_r($prato);
                    echo '</pre>'; */
                    formatar_produto($prato);
                }
                ?>
            </div>
        </div>
        
        <button class='other_options_button'><a href="<?php echo esc_url( get_permalink( wc_get_page_id( 'shop' ))); ?>">Veja outras opções</a></button>
    </section>
    <section class='lp_section3'>
        <h2>VISITE NOSSA LOJA FÍSICA</h2>
        <div>
            <div class='map_div'>
                <iframe frameborder="0" width="345" height="205" src="https://www.google.com/maps/embed/v1/place?q=UFF+-+Praia+Vermelha+Campus+-+Rua+Passo+da+Pátria+-+São+Domingos,+Niterói+-+RJ,+Brasil&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe>
                <div class='infos_div adress'>
                    <img src="<?php echo IMAGES_DIR . '/icone-endereco.png' ?>" alt="Ícone de endereço">
                    <p>Rua Passo da Pátria, São Domingos, Niterói, RJ</p>
                </div>
                <div class='infos_div telephone'>
                    <img src="<?php echo IMAGES_DIR . '/icone-telefone.png' ?>" alt="ícone de telefone">
                    <p>(99) 99999-9999</p>
                </div>


                <!-- <div style="overflow:hidden;resize:none;max-width:100%;width:345px;height:205px;"><div id="embed-map-display" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=UFF+-+Praia+Vermelha+Campus+-+Rua+Passo+da+Pátria+-+São+Domingos,+Niterói+-+RJ,+Brasil&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe></div><a class="the-googlemap-enabler" href="https://www.bootstrapskins.com/themes" id="authmaps-data">premium bootstrap themes</a><style>#embed-map-display img{max-height:none;max-width:none!important;background:none!important;}</style></div> -->
            </div>
            <img src="<?php echo IMAGES_DIR . '/footer-imagem.png'; ?>" alt="Imagem da empresa">
        </div>
    </section>
</main>

<?php
get_footer();
?>